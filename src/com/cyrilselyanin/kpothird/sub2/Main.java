package com.cyrilselyanin.kpothird.sub2;

public class Main {

    public static void main(String[] args) {
        Double k = calcModulesSwCount(6060);
        System.out.println("Число модулей программного средства:");
        System.out.println(String.format("%d шт.", k.intValue()));

        Double n = calcProgramLength(k);
        System.out.println("Длина программы:");
        System.out.println(String.format("%d с.", n.intValue()));

        Double v = calcSwV(k);
        System.out.println("Объем программного обеспечения:");
        System.out.println(String.format("%d бит", v.intValue()));

        Double p = calcAsmCommandsCount(n);
        System.out.println("Количество команд ассемблера:");
        System.out.println(String.format("%d шт.", p.intValue()));

        Double tk = calcProgrammingTime(n);
        System.out.println("Календарное время программирования:");
        System.out.println(String.format("%.2f д.", tk));

        Double b = calcErrorsCount(v);
        System.out.println("Потенциальное количество ошибок:");
        System.out.println(String.format("%d шт.", b.intValue()));

        Double tn = calcSwReliability(tk, b);
        System.out.println("Начальная надёжность ПО (времени наработки на отказ):");
        System.out.println(String.format("%.2f ч.", tn));
    }

    // Число модулей программного средства
    public static Double calcModulesSwCount(Integer paramCount) {
        double k = paramCount.doubleValue() / 8;
        if (k <= 8) return k;
        // Структура ПО будет многоуровневой, число модулей рассчитывается так
        return (paramCount.doubleValue() / 8) + (paramCount.doubleValue() / Math.pow(8, 2));
    }

    // Расчет длины программы
    public static Double calcProgramLength(Double k) {
        return (220 * k) + k * calcLog(2.0, k);
    }

    // Расчет объема программного обеспечения
    public static Double calcSwV(Double k) {
        return k * 220 * calcLog(2.0, 48.0);
    }

    // Расчет количества команд ассемблера
    public static Double calcAsmCommandsCount(Double n) {
        return (3 * n) / 8;
    }

    // Расчет календарного времени программирования
    public static Double calcProgrammingTime(Double n) {
        double m = 3.0;
        double v = 15.0;
        return (3 * n) / (8 * m * v);
    }

    // Расчет потенциального количества ошибок
    public static Double calcErrorsCount(Double v) {
        return v / 3000.0;
    }

    // Расчет начальной надёжности ПО (времени наработки на отказ)
    public static Double calcSwReliability(Double tk, Double b) {
        double tkWork = tk * 8;
        return tkWork / (2 * Math.log(b));
    }

    // Вычисление логарифма
    public static Double calcLog(Double base, Double logNumber) {
        return Math.log(logNumber) / Math.log(base);
    }

}
